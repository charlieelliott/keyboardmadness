//
//  MLog.h
//  KeyMadness
//
//  Created by Charlie Elliott on 11/22/14.
//  Copyright (c) 2014 Hidden World Hut. All rights reserved.
//

#define MLogf(_fmt, ...) m_LOGprintf((@"%@ %@ " _fmt), NSStringFromClass([self class]), NSStringFromSelector(_cmd), ##__VA_ARGS__);

NS_INLINE void m_LOGprintf(NSString *format, ...)
{
    va_list args;
    va_start(args, format);
    
    NSString *message = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    
    printf("%s\n", [message UTF8String]);
}
