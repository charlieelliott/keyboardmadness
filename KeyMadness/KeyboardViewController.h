//
//  KeyboardViewController.h
//  KeyMadness
//
//  Created by Charlie Elliott on 11/22/14.
//  Copyright (c) 2014 charlieelliott. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KeyboardViewController : UIInputViewController

@end
