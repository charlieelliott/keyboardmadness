//
//  AppDelegate.h
//  KeyboardMadness
//
//  Created by Charlie Elliott on 11/22/14.
//  Copyright (c) 2014 charlieelliott. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

